#include "DatabaseHandler.h"

using namespace std;

class NotConnectedException: public exception
{
    virtual const char* what() const throw() {
        return "Not connected";
    }
} not_connected;


TDatabaseHandler::TDatabaseHandler(TDatabaseHandlerConfig config_struct)
{
    try {
        string command = "dbname = " + config_struct.db_name + " user = " + config_struct.db_user + " password = " + config_struct.db_userpw +
                            " hostaddr = " + config_struct.hostaddr + " port = " + to_string(config_struct.port);

        db_connect = new pqxx::connection(command);
        if (db_connect->is_open()) {
            cout << "Opened database successfully: " << db_connect->dbname() << endl;
            is_connected = true;
        } else {
            cout << "Can't open database" << endl;
        }
    } catch (const exception &e) {
        cerr << e.what() << std::endl;
    }    
}

TDatabaseHandler::~TDatabaseHandler()
{
    cout << "disconnect db"<< endl;
    if (db_connect != NULL) {

        if (db_connect->is_open()) {
            db_connect->disconnect ();
        }
        delete(db_connect);
    }
}

TDatabaseHandlerReturn TDatabaseHandler::ExecuteQuerry(string command, bool isTransactional)
{
    if (!is_connected) {
        throw not_connected;
        return EmtpyReturn;
    }

    if (isTransactional) {
        return ExecuteTransactionalQuerry(command);
    } else {
        return ExecuteNonTransactionalQuerry(command);
    }
}


TDatabaseHandlerReturn TDatabaseHandler::ExecuteTransactionalQuerry(string command)
{
    try {
        pqxx::work W(*db_connect);
        pqxx::result R(W.exec(command.c_str()));
        W.commit();
        return TDatabaseHandler::PQResultToJson(&R);

    } catch (const exception &e) {
        cerr << e.what() << std::endl;
    }

    return EmtpyReturn;
}

TDatabaseHandlerReturn TDatabaseHandler::ExecuteNonTransactionalQuerry(string command)
{
    
    try {
        pqxx::nontransaction N(*db_connect);
        pqxx::result R(N.exec(command.c_str()));
        for (pqxx::result::const_iterator c = R.begin(); c != R.end(); ++c) {
      }
      return TDatabaseHandler::PQResultToJson(&R);

    } catch (const exception &e) {
        cerr << e.what() << std::endl;
    }

   return EmtpyReturn;
}


TDatabaseHandlerReturn TDatabaseHandler::PQResultToJson(const pqxx::result* R)
{
    Json::Value root;
    for (pqxx::result::const_iterator c = R->begin(); c != R->end(); ++c) {
        Json::Value subroot;
        for(int i = 0; i < R->columns(); i++) {
            subroot[c[i].name()] = c[i].as<string>();
        }
        root.append(subroot);
    }

    return TDatabaseHandlerReturn(root.toStyledString());
}