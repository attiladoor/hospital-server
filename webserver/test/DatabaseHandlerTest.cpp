#include <limits.h>
#include <iostream>
#include <json/json.h>
#include "gtest/gtest.h"
#include "DatabaseHandler.h"

#define private public

class DatabaseHanderTest : public ::testing::Test {
    protected:
        TDatabaseHandler* db;
    virtual void SetUp() {
        TDatabaseHandlerConfig config; 
        db = new TDatabaseHandler(config);
     }
    virtual void TearDown() {
        delete db;
     }
};

TEST_F(DatabaseHanderTest,ExecuteNonTransactionalQuerry){
    auto result = db->ExecuteQuerry("SELECT * FROM patients", false);
    EXPECT_EQ(result.getIsSuccessful(),true);
}

TEST_F(DatabaseHanderTest,ParseExecuteNonTransactionalQuerry){
    auto result = db->ExecuteQuerry("SELECT * FROM patients", false);
    Json::Value root;
    Json::Reader reader;
    reader.parse(result.getStringReturn(), root);
    EXPECT_EQ(root[0]["first_name"], "Victor");
}
